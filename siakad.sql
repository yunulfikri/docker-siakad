-- MySQL dump 10.13 Distrib 5.7.38, for Linux (x86_64)
--
-- Host: localhost Database: siakad
-- ------------------------------------------------------
-- Server version 5.7.38-0ubuntu0.18.04.1 /*!40101


DROP TABLE IF EXISTS `absen_siswas`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `absen_siswas` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `siswa_id` int(11) NOT NULL, `jadwal_id` int(11) NOT NULL, `ket` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `tanggal` date NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 21 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `absen_siswas`
-- LOCK TABLES `absen_siswas` WRITE; /*!40000 ALTER TABLE `absen_siswas` DISABLE KEYS */;
INSERT INTO `absen_siswas` VALUES (1, 3, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (2, 4, 1, 'izin', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (3, 5, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (4, 6, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (5, 7, 1, 'sakit', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (6, 8, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (7, 9, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (8, 10, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (9, 11, 1, 'tanpa keterangan', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (10, 12, 1, 'hadir', '2022-06-20', '2022-06-20 15:19:15', '2022-06-20 15:19:15'), (11, 3, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (12, 4, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (13, 5, 1, 'sakit', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (14, 6, 1, 'izin', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (15, 7, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (16, 8, 1, 'tanpa keterangan', '2022-06-29', '2022-06-29 10:34:39', '2022-06-29 10:34:39'), (17, 9, 1, 'tanpa keterangan', '2022-06-29', '2022-06-29 10:34:40', '2022-06-29 10:34:40'), (18, 10, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:40', '2022-06-29 10:34:40'), (19, 11, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:40', '2022-06-29 10:34:40'), (20, 12, 1, 'hadir', '2022-06-29', '2022-06-29 10:34:40', '2022-06-29 10:34:40'); /*!40000 ALTER TABLE `absen_siswas` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `absensi_guru`
--

DROP TABLE IF EXISTS `absensi_guru`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `absensi_guru` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `tanggal` date NOT NULL, `guru_id` int(11) NOT NULL, `kehadiran_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `absensi_guru`
-- LOCK TABLES `absensi_guru` WRITE; /*!40000 ALTER TABLE `absensi_guru` DISABLE KEYS */;
INSERT INTO `absensi_guru` VALUES (1, '2022-06-20', 5, 5, '2022-06-20 15:01:00', '2022-06-20 15:01:00'), (2, '2022-06-29', 5, 5, '2022-06-29 10:33:39', '2022-06-29 10:33:39'); /*!40000 ALTER TABLE `absensi_guru` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `failed_jobs` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `connection` text COLLATE utf8mb4_unicode_ci NOT NULL, `queue` text COLLATE utf8mb4_unicode_ci NOT NULL, `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL, `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL, `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `failed_jobs`
-- LOCK TABLES `failed_jobs` WRITE; /*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */; /*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `guru`
--

DROP TABLE IF EXISTS `guru`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `guru` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `id_card` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL, `nip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `nama_guru` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `mapel_id` int(11) DEFAULT NULL, `kode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `jk` enum('L', 'P') COLLATE utf8mb4_unicode_ci NOT NULL, `telp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `tmp_lahir` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `tgl_lahir` date DEFAULT NULL, `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 15 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `guru`
-- LOCK TABLES `guru` WRITE; /*!40000 ALTER TABLE `guru` DISABLE KEYS */;
INSERT INTO `guru` VALUES (3, '00001', '19660412 198803 2 024', 'Hj. Fetti Fatimah, S. pd', NULL, 'FT', 'P', '0', 'Banda Aceh', '1966-04-12', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 09:57:29', '2022-06-20 09:57:29', NULL), (4, '00002', '196710291994032002', 'Ir. Hj. Ulfah Sari', NULL, 'US', 'P', '0', 'Aceh Selatan', '1967-10-29', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 10:04:10', '2022-06-20 10:04:10', NULL), (5, '00003', '197209032002122003', 'Caya Murni,S.Pd', NULL, 'CM', 'P', '0', 'Aceh Tengah', '1972-09-30', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 10:06:47', '2022-06-20 10:06:47', NULL), (6, '00004', '197312312000082003', 'Hj. Marlina, S. Pd', NULL, 'MAR', 'P', '0', 'Aceh Selatan', '1973-12-31', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 10:08:34', '2022-06-20 10:08:34', NULL), (7, '00005', '197506252003121008', 'Saifullah, S. Pd. M. Pd', NULL, 'SH', 'P', '0', 'Paloh', '1975-06-25', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 10:11:49', '2022-06-20 10:11:49', NULL), (8, '00006', '196712312002121024', 'Drs. A. Wahab', NULL, 'AW', 'L', '0', 'Blang Reuling', '1967-12-31', 'uploads/guru/35251431012020_male.jpg', '2022-06-20 10:20:59', '2022-06-20 10:20:59', NULL), (9, '00007', '196308251991032004', 'Wardiani Ahmad, S. Pd.I', NULL, 'WAR', 'P', '0', 'Pagar Air', '1963-08-25', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 10:23:57', '2022-06-20 10:23:57', NULL), (10, '00008', '197607062005042005', 'Cut Milawati, S.si', NULL, 'CL', 'P', '0', 'Kuta Buloh I', '1976-07-06', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 12:50:25', '2022-06-20 12:50:25', NULL), (11, '00009', '197505292006042006', 'Nazariah, S.Pd', NULL, 'NZ', 'P', '0', 'Pidie', '1975-05-29', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 12:52:07', '2022-06-20 12:52:07', NULL), (12, '00010', '1997', 'Rika Ulfiyanti, S.Pd', NULL, 'RK', 'P', '0', 'Aceh Selatan', '1997-04-08', 'uploads/guru/23171022042020_female.jpg', '2022-06-20 12:58:06', '2022-06-20 12:58:06', NULL), (13, '00011', '1995', 'Fauzi, ST', NULL, 'FZ', 'L', '0', 'Banda Aceh', '1995-07-09', 'uploads/guru/35251431012020_male.jpg', '2022-06-20 13:08:08', '2022-06-20 13:08:08', NULL), (14, '00012', '5155', 'Pak Yunul', NULL, 'G2A', 'L', '08854455545', 'Banda Aceh', NULL, 'uploads/guru/35251431012020_male.jpg', '2022-07-02 08:53:46', '2022-07-02 08:53:46', NULL); /*!40000 ALTER TABLE `guru` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `guru_mapels`
--

DROP TABLE IF EXISTS `guru_mapels`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `guru_mapels` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `guru_id` int(11) NOT NULL, `mapel_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 20 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `guru_mapels`
-- LOCK TABLES `guru_mapels` WRITE; /*!40000 ALTER TABLE `guru_mapels` DISABLE KEYS */;
INSERT INTO `guru_mapels` VALUES 
(1, 1, 1, '2022-06-20 09:22:15', '2022-06-20 09:22:15', NULL), 
(2, 1, 1, '2022-06-20 09:22:15', '2022-06-20 09:22:15', NULL), 
(3, 2, 1, '2022-06-20 09:36:40', '2022-06-20 09:36:40', NULL), 
(4, 3, 1, '2022-06-20 09:57:29', '2022-06-20 09:57:29', NULL), 
(5, 4, 1, '2022-06-20 10:04:11', '2022-06-20 10:04:11', NULL), 
(6, 5, 1, '2022-06-20 10:06:47', '2022-06-20 10:06:47', NULL), 
(7, 5, 1, '2022-06-20 10:06:47', '2022-06-20 10:06:47', NULL), 
(8, 6, 1, '2022-06-20 10:08:34', '2022-06-20 10:08:34', NULL), 
(9, 7, 1, '2022-06-20 10:11:49', '2022-06-20 10:11:49', NULL), 
(10, 7, 1, '2022-06-20 10:11:49', '2022-06-20 10:11:49', NULL), 
(11, 8, 1, '2022-06-20 10:20:59', '2022-06-20 10:20:59', NULL), 
(12, 9, 1, '2022-06-20 10:23:57', '2022-06-20 10:23:57', NULL), 
(13, 10, 1, '2022-06-20 12:50:25', '2022-06-20 12:50:25', NULL), 
(14, 10, 1, '2022-06-20 12:50:25', '2022-06-20 12:50:25', NULL), 
(15, 11, 1, '2022-06-20 12:52:07', '2022-06-20 12:52:07', NULL), 
(16, 12, 1, '2022-06-20 12:58:06', '2022-06-20 12:58:06', NULL), 
(17, 13, 1, '2022-06-20 13:08:08', '2022-06-20 13:08:08', NULL), 
(18, 13, 1, '2022-06-20 13:08:08', '2022-06-20 13:08:08', NULL), 
(19, 14, 1, '2022-07-02 08:53:46', '2022-07-02 08:53:46', NULL); /*!40000 ALTER TABLE `guru_mapels` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `hari`
--

DROP TABLE IF EXISTS `hari`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `hari` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `nama_hari` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `hari`
-- LOCK TABLES `hari` WRITE; /*!40000 ALTER TABLE `hari` DISABLE KEYS */;
INSERT INTO `hari` VALUES (1, 'Senin', '2022-04-24 20:44:43', '2022-04-24 20:44:43'), (2, 'Selasa', '2022-04-24 20:44:43', '2022-04-24 20:44:43'), (3, 'Rabu', '2022-04-24 20:44:43', '2022-04-24 20:44:43'), (4, 'Kamis', '2022-04-24 20:44:43', '2022-04-24 20:44:43'), (5, 'Jumat','2022-04-24 20:44:43','2022-04-24 20:44:43'); /*!40000 ALTER TABLE `hari` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `jadwal`
--

DROP TABLE IF EXISTS `jadwal`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `jadwal` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `hari_id` int(11) NOT NULL, `kelas_id` int(11) NOT NULL, `mapel_id` int(11) NOT NULL, `guru_id` int(11) NOT NULL, `jam_mulai` time NOT NULL, `jam_selesai` time NOT NULL, `ruang_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `jadwal`
-- LOCK TABLES `jadwal` WRITE; /*!40000 ALTER TABLE `jadwal` DISABLE KEYS */;
INSERT INTO `jadwal` VALUES (1,4,5,15,5,'15:16:00','15:45:00',1,'2022-06-20 15:08:15','2022-06-20 15:15:27',NULL),(2,1,5,1,11,'11:00:00','11:45:00',2,'2022-06-29 23:04:45','2022-06-29 23:04:45',NULL); /*!40000 ALTER TABLE `jadwal` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `kehadiran`
--

DROP TABLE IF EXISTS `kehadiran`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `kehadiran` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `ket` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL, `color` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `kehadiran`
-- LOCK TABLES `kehadiran` WRITE; /*!40000 ALTER TABLE `kehadiran` DISABLE KEYS */;
INSERT INTO `kehadiran` VALUES (1,'Hadir','3C0','2022-04-24 20:44:43','2022-04-24 20:44:43'),(2,'Izin','0CF','2022-04-24 20:44:43','2022-04-24 20:44:43'),(3,'Bertugas Keluar','F90','2022-04-24 20:44:43','2022-04-24 20:44:43'),(4,'Sakit','FF0','2022-04-24 20:44:43','2022-04-24 20:44:43'),(5,'Terlambat','7F0','2022-04-24 20:44:44','2022-04-24 20:44:44'),(6,'Tanpa Keterangan','F00','2022-04-24 20:44:44','2022-04-24 20:44:44'); /*!40000 ALTER TABLE `kehadiran` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `kelas`
--

DROP TABLE IF EXISTS `kelas`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `kelas` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `nama_kelas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `paket_id` int(11) NOT NULL, `guru_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `kelas`
-- LOCK TABLES `kelas` WRITE; /*!40000 ALTER TABLE `kelas` DISABLE KEYS */;
INSERT INTO `kelas` VALUES (1,'XII NKPI',1,4,'2022-05-14 12:09:18','2022-05-14 12:09:18',NULL),(2,'X TBSM (A)',3,16,'2022-05-14 12:10:36','2022-05-14 12:10:36',NULL),(3,'XI TKR',2,9,'2022-05-14 12:11:13','2022-05-14 12:11:13',NULL),(4,'XII TBSM',3,11,'2022-06-20 13:04:34','2022-06-20 13:04:34',NULL),(5,'X TBSM (B)',3,13,'2022-06-20 13:09:18','2022-06-20 13:09:18',NULL); /*!40000 ALTER TABLE `kelas` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `kurikulums`
--

DROP TABLE IF EXISTS `kurikulums`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `kurikulums` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `kurikulums`
-- LOCK TABLES `kurikulums` WRITE; /*!40000 ALTER TABLE `kurikulums` DISABLE KEYS */;
INSERT INTO `kurikulums` VALUES (1,'Program Tahunan','uploads/kurikulum/41361021062022_2_7_Program Tahunan.docx','2022-06-21 10:36:41','2022-06-21 10:36:41'); /*!40000 ALTER TABLE `kurikulums` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `mapel`
--

DROP TABLE IF EXISTS `mapel`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `mapel` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `nama_mapel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `paket_id` int(11) NOT NULL, `kelompok` enum('A','B','C') COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 28 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `mapel`
-- LOCK TABLES `mapel` WRITE; /*!40000 ALTER TABLE `mapel` DISABLE KEYS */;
INSERT INTO `mapel` VALUES 
(1,'Matematika',9,'A','2022-05-12 20:03:51','2022-05-12 20:03:51',NULL),
(2,'Simulasi Digital',9,'A','2022-05-12 20:05:38','2022-05-12 20:05:38',NULL),
(3,'Kimia',9,'A','2022-05-12 20:08:59','2022-05-12 20:08:59',NULL),
(4,'Fisika',9,'A','2022-05-12 20:11:04','2022-05-12 20:11:04',NULL),
(5,'Bahasa Inggris',9,'A','2022-05-12 20:12:47','2022-05-12 20:13:44',NULL),
(6,'Bahasa Indonesia',9,'A','2022-05-12 20:12:50','2022-06-20 09:00:32',NULL),
(7,'Pend Jasmani, Olah Raga & Kesehatan',9,'A','2022-05-12 20:17:25','2022-05-12 20:17:25',NULL),
(8,'Sejarah Indonesia',9,'A','2022-05-12 20:18:24','2022-05-12 20:18:24',NULL),
(9,'Pendidikan Agama & Budi Pekerti',9,'A','2022-05-12 20:19:32','2022-05-12 20:19:32',NULL),
(10,'Seni Budaya',9,'A','2022-05-12 20:21:41','2022-05-12 20:21:41',NULL),
(11,'Produk Kreativitas & Kewirausahaan',9,'A','2022-05-12 20:22:40','2022-05-12 20:22:40',NULL),
(12,'Pekerjaan Dasar Teknik Otomotif',2,'C','2022-05-12 20:25:23','2022-05-12 20:25:23',NULL),
(13,'Olah Gerak dan Pengendalian Kapal Penangkapan Ikan',1,'C','2022-05-12 20:27:09','2022-05-12 20:28:25',NULL),
(14,'Biologi Perikanan',1,'C','2022-05-12 20:30:19','2022-05-12 20:30:19',NULL),
(15,'Pengelolaan Bengkel Sepeda Motor',3,'C','2022-05-12 20:31:26','2022-05-12 20:31:26',NULL),(16,'Pemeliharaan Mesin Sepeda Motor',3,'C','2022-05-12 20:33:17','2022-05-12 20:33:17',NULL),(17,'Teknik Dasar Otomotif',2,'C','2022-05-12 20:33:57','2022-05-12 20:33:57',NULL),(18,'Pemeliharaan Mesin Kendaraan Ringan',2,'C','2022-05-12 20:35:46','2022-05-12 20:35:46',NULL),(19,'Pemeliharaan Kelistrikan Sepeda Motor',3,'C','2022-05-12 20:38:00','2022-05-12 20:38:00',NULL),(20,'Pemeliharaan Sasis Sepeda Motor',3,'C','2022-05-12 20:39:01','2022-05-12 20:39:01',NULL),(21,'Gambar Teknik Otomotif',2,'C','2022-05-12 20:40:19','2022-05-12 20:40:19',NULL),(22,'Permesinan Kapal Penangkapan Ikan',1,'C','2022-05-12 20:41:27','2022-05-12 20:41:27',NULL),(23,'Bahasa Inggris Maritim',1,'C','2022-05-12 20:42:39','2022-05-12 20:42:39',NULL),(24,'Ilmu Pelayaran Datar',1,'C','2022-05-12 20:43:04','2022-05-12 20:43:04',NULL),(25,'Bimbingan Konseling',9,'A','2022-05-14 11:19:17','2022-05-14 11:19:17',NULL),(26,'Pendidikan Pancasila & Kewarganegaraan',9,'A','2022-05-14 11:41:35','2022-05-14 11:41:35',NULL),(27,'Navigasi Radar dan Elektronik',9,'C','2022-05-14 11:57:58','2022-05-14 11:57:58',NULL); /*!40000 ALTER TABLE `mapel` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `migrations`
--

DROP TABLE IF EXISTS `migrations`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `migrations` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT, `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `batch` int(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 41 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `migrations`
-- LOCK TABLES `migrations` WRITE; /*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (20,'2014_10_12_000000_create_users_table',1),(21,'2014_10_12_100000_create_password_resets_table',1),(22,'2019_08_19_000000_create_failed_jobs_table',1),(23,'2020_03_12_092809_create_hari_table',1),(24,'2020_03_12_092854_create_guru_table',1),(25,'2020_03_12_092926_create_absensi_guru_table',1),(26,'2020_03_12_092941_create_jadwal_table',1),(27,'2020_03_12_092953_create_kehadiran_table',1),(28,'2020_03_12_093010_create_kelas_table',1),(29,'2020_03_12_093018_create_mapel_table',1),(30,'2020_03_12_093027_create_nilai_table',1),(31,'2020_03_12_093036_create_paket_table',1),(32,'2020_03_12_093050_create_pengumuman_table',1),(33,'2020_03_12_093102_create_rapot_table',1),(34,'2020_03_12_093117_create_ruang_table',1),(35,'2020_03_12_093130_create_siswa_table',1),(36,'2020_03_16_102220_create_ulangan_table',1),(37,'2020_04_07_094355_create_sikap_table',1),(38,'2022_02_08_181935_create_kurikulums_table',1),(39,'2022_05_17_150245_create_guru_mapels_table',2),(40,'2022_05_19_113219_create_absen_siswas_table',2); /*!40000 ALTER TABLE `migrations` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `nilai`
--

DROP TABLE IF EXISTS `nilai`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `nilai` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `guru_id` int(11) NOT NULL, `kkm` int(11) NOT NULL DEFAULT '70', `deskripsi_a` text COLLATE utf8mb4_unicode_ci, `deskripsi_b` text COLLATE utf8mb4_unicode_ci, `deskripsi_c` text COLLATE utf8mb4_unicode_ci, `deskripsi_d` text COLLATE utf8mb4_unicode_ci, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `nilai`
-- LOCK TABLES `nilai` WRITE; /*!40000 ALTER TABLE `nilai` DISABLE KEYS */;
INSERT INTO `nilai` VALUES 
(1,3,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(2,4,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(3,5,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(4,6,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(5,7,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(6,8,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(7,9,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(8,10,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(9,11,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(10,12,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(11,13,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'),
(12,14,70,NULL,NULL,NULL,NULL,'2022-07-02 08:53:46','2022-07-02 08:53:46'); /*!40000 ALTER TABLE `nilai` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `paket`
--

DROP TABLE IF EXISTS `paket`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `paket` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `ket` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 4 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `paket`
-- LOCK TABLES `paket` WRITE; /*!40000 ALTER TABLE `paket` DISABLE KEYS */;
INSERT INTO `paket` VALUES (1,'Nautika Kapal Penangkap Ikan','2022-04-24 20:44:44','2022-04-24 20:44:44'),(2,'Teknik Kendaraan Ringan Otomotif','2022-04-24 20:44:44','2022-04-24 20:44:44'),(3,'Teknik Bisnis Sepeda Motor','2022-04-24 20:44:44','2022-04-24 20:44:44'); /*!40000 ALTER TABLE `paket` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `password_resets`
--

DROP TABLE IF EXISTS `password_resets`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `password_resets` ( `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, KEY `password_resets_email_index` (`email`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `password_resets`
-- LOCK TABLES `password_resets` WRITE; /*!40000 ALTER TABLE `password_resets` DISABLE KEYS */; /*!40000 ALTER TABLE `password_resets` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `pengumuman`
--

DROP TABLE IF EXISTS `pengumuman`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `pengumuman` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `opsi` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL, `isi` text COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `pengumuman`
-- LOCK TABLES `pengumuman` WRITE; /*!40000 ALTER TABLE `pengumuman` DISABLE KEYS */;
INSERT INTO `pengumuman` VALUES (1,'pengumuman','Libur Idul Adha dimulai dari tgl 06 juli 2022 s/d 13 juli 2022','2022-04-24 20:44:44','2022-06-20 14:57:29'); /*!40000 ALTER TABLE `pengumuman` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `rapot`
--

DROP TABLE IF EXISTS `rapot`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `rapot` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `siswa_id` int(11) NOT NULL, `kelas_id` int(11) NOT NULL, `guru_id` int(11) NOT NULL, `mapel_id` int(11) NOT NULL, `p_nilai` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL, `p_predikat` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL, `p_deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL, `k_nilai` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `k_predikat` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `k_deskripsi` text COLLATE utf8mb4_unicode_ci, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 12 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `rapot`
-- LOCK TABLES `rapot` WRITE; /*!40000 ALTER TABLE `rapot` DISABLE KEYS */;
INSERT INTO `rapot` VALUES (1,13,5,5,15,'90','B','Baik','90','B','Baik','2022-06-30 16:11:47','2022-06-30 16:12:34'),(2,3,5,5,15,'84','B','Baik','90','B','Baik','2022-06-30 16:12:01','2022-06-30 16:12:38'),(3,4,5,5,15,'82','B','Baik','89','B','Baik','2022-06-30 16:12:09','2022-07-01 23:35:02'),(4,5,5,5,15,'82','B','Baik','88','B','Baik','2022-06-30 16:12:10','2022-07-01 23:35:08'),(5,6,5,5,15,'87','B','Baik','90','B','Baik','2022-06-30 16:12:11','2022-07-01 23:35:17'),(6,7,5,5,15,'91','A','Sangat Baik','98','A','Sangat Baik','2022-06-30 16:12:11','2022-07-01 23:35:22'),(7,8,5,5,15,'83','B','Baik','88','B','Baik','2022-06-30 16:12:14','2022-07-01 23:35:30'),(8,9,5,5,15,'75','C','Cukup','85','B','Baik','2022-06-30 16:12:15','2022-07-01 23:35:35'),(9,10,5,5,15,'81','B','Baik','89','B','Baik','2022-06-30 16:12:17','2022-07-01 23:35:41'),(10,11,5,5,15,'81','B','Baik','86','B','Baik','2022-06-30 16:12:19','2022-07-01 23:35:52'),(11,12,5,5,15,'84','B','Baik','86','B','Baik','2022-06-30 16:12:21','2022-07-01 23:35:59'); /*!40000 ALTER TABLE `rapot` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `ruang`
--

DROP TABLE IF EXISTS `ruang`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `ruang` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `nama_ruang` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 41 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `ruang`
-- LOCK TABLES `ruang` WRITE; /*!40000 ALTER TABLE `ruang` DISABLE KEYS */;
INSERT INTO `ruang` VALUES (1,'Ruang 01','2022-04-24 20:44:44','2022-04-24 20:44:44'),(2,'Ruang 02','2022-04-24 20:44:44','2022-04-24 20:44:44'),(3,'Ruang 03','2022-04-24 20:44:44','2022-04-24 20:44:44'),(4,'Ruang 04','2022-04-24 20:44:44','2022-04-24 20:44:44'),(5,'Ruang 05','2022-04-24 20:44:44','2022-04-24 20:44:44'),(6,'Ruang 06','2022-04-24 20:44:44','2022-04-24 20:44:44'),(7,'Ruang 07','2022-04-24 20:44:45','2022-04-24 20:44:45'),(8,'Ruang 08','2022-04-24 20:44:45','2022-04-24 20:44:45'),(9,'Ruang 09','2022-04-24 20:44:45','2022-04-24 20:44:45'),(10,'Ruang 10','2022-04-24 20:44:45','2022-04-24 20:44:45'),(11,'Ruang 11','2022-04-24 20:44:45','2022-04-24 20:44:45'),(12,'Ruang 12','2022-04-24 20:44:45','2022-04-24 20:44:45'),(13,'Ruang 13','2022-04-24 20:44:45','2022-04-24 20:44:45'),(14,'Ruang 14','2022-04-24 20:44:45','2022-04-24 20:44:45'),(15,'Ruang 15','2022-04-24 20:44:45','2022-04-24 20:44:45'),(16,'Ruang 16','2022-04-24 20:44:46','2022-04-24 20:44:46'),(17,'Ruang 17','2022-04-24 20:44:46','2022-04-24 20:44:46'),(18,'Ruang 18','2022-04-24 20:44:46','2022-04-24 20:44:46'),(19,'Ruang 19','2022-04-24 20:44:46','2022-04-24 20:44:46'),(20,'Ruang 20','2022-04-24 20:44:48','2022-04-24 20:44:48'),(21,'Ruang 21','2022-04-24 20:44:48','2022-04-24 20:44:48'),(22,'Ruang 22','2022-04-24 20:44:48','2022-04-24 20:44:48'),(23,'Ruang 23','2022-04-24 20:44:48','2022-04-24 20:44:48'),(24,'Ruang 24','2022-04-24 20:44:48','2022-04-24 20:44:48'),(25,'Ruang 25','2022-04-24 20:44:48','2022-04-24 20:44:48'),(26,'Ruang 26','2022-04-24 20:44:48','2022-04-24 20:44:48'),(27,'Ruang 27','2022-04-24 20:44:48','2022-04-24 20:44:48'),(28,'Ruang 28','2022-04-24 20:44:48','2022-04-24 20:44:48'),(29,'Ruang 29','2022-04-24 20:44:48','2022-04-24 20:44:48'),(30,'Ruang 30','2022-04-24 20:44:48','2022-04-24 20:44:48'),(31,'Ruang 31','2022-04-24 20:44:48','2022-04-24 20:44:48'),(32,'Ruang 32','2022-04-24 20:44:48','2022-04-24 20:44:48'),(33,'Ruang 33','2022-04-24 20:44:48','2022-04-24 20:44:48'),(34,'Ruang 34','2022-04-24 20:44:48','2022-04-24 20:44:48'),(35,'Ruang 35','2022-04-24 20:44:48','2022-04-24 20:44:48'),(36,'Ruang 36','2022-04-24 20:44:48','2022-04-24 20:44:48'),(37,'Ruang 37','2022-04-24 20:44:48','2022-04-24 20:44:48'),(38,'Ruang 38','2022-04-24 20:44:48','2022-04-24 20:44:48'),(39,'Ruang 39','2022-04-24 20:44:48','2022-04-24 20:44:48'),(40,'Ruang 40','2022-04-24 20:44:48','2022-04-24 20:44:48'); /*!40000 ALTER TABLE `ruang` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `sikap`
--

DROP TABLE IF EXISTS `sikap`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `sikap` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `siswa_id` int(11) NOT NULL, `kelas_id` int(11) NOT NULL, `guru_id` int(11) NOT NULL, `mapel_id` int(11) NOT NULL, `sikap_1` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `sikap_2` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `sikap_3` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `sikap`
-- LOCK TABLES `sikap` WRITE; /*!40000 ALTER TABLE `sikap` DISABLE KEYS */; /*!40000 ALTER TABLE `sikap` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `siswa`
--

DROP TABLE IF EXISTS `siswa`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `siswa` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `no_induk` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL, `nis` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `nama_siswa` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL, `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL, `telp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `tmp_lahir` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `tgl_lahir` date DEFAULT NULL, `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `kelas_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB AUTO_INCREMENT = 14 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `siswa`
-- LOCK TABLES `siswa` WRITE; /*!40000 ALTER TABLE `siswa` DISABLE KEYS */;
INSERT INTO `siswa` VALUES (1,'041300','061802223','Ahmad Zaki','L','0','-','2006-01-08','uploads/siswa/52471919042020_male.jpg',2,'2022-05-14 12:15:45','2022-05-14 12:15:45',NULL),(2,'041301','0061435652','Alfi Maulana','L','0','-','2006-01-04','uploads/siswa/52471919042020_male.jpg',2,'2022-05-14 12:17:03','2022-05-14 12:17:03',NULL),(3,'041323','0061892286','M. Yasir','L','0','-','2007-01-01','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 13:18:25','2022-06-20 13:18:25',NULL),(4,'041325','0064595208','Muawal Difaq','L','0','-','2007-02-02','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 13:19:59','2022-06-20 13:19:59',NULL),(5,'041326','0059991424','Muhammad Afrizal','L','0','-','2007-03-03','uploads/siswa/50271431012020_female.jpg',5,'2022-06-20 14:45:18','2022-06-20 14:47:00',NULL),(6,'041329','0068547730','Muhammad Ramadhani','L','0','-','2007-04-04','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:46:43','2022-06-20 14:46:43',NULL),(7,'041331','0068568279','Muhammad Syakir Rafi','L','0','-','2007-05-05','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:48:52','2022-06-20 14:48:52',NULL),(8,'041334','0051397135','Rahmat Mulia','L','0','-','2007-06-06','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:50:16','2022-06-20 14:50:16',NULL),(9,'041335','0064876162','Riki Munandar','L','0','-','2007-07-07','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:51:29','2022-06-20 14:51:29',NULL),(10,'041337','0068157926','Rizky Muliawan','L','0','-','2008-08-08','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:52:56','2022-06-20 14:52:56',NULL),(11,'041339','0064426894','Sayed Imran','L','0','-','2007-09-09','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:54:14','2022-06-20 14:54:14',NULL),(12,'041342','0069520731','Tajul Fudzari','L','0','-','2007-10-10','uploads/siswa/52471919042020_male.jpg',5,'2022-06-20 14:55:26','2022-06-20 14:55:26',NULL),(13,'11999','111999','Yunul Fikri','L',NULL,'Banda Aceh','2022-06-22','uploads/siswa/52471919042020_male.jpg',5,'2022-06-30 16:09:46','2022-06-30 16:09:46',NULL); /*!40000 ALTER TABLE `siswa` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `ulangan`
--

DROP TABLE IF EXISTS `ulangan`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `ulangan` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `siswa_id` int(11) NOT NULL, `kelas_id` int(11) NOT NULL, `guru_id` int(11) NOT NULL, `mapel_id` int(11) NOT NULL, `ulha_1` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `ulha_2` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `uts` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `ulha_3` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `uas` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `ulangan`
-- LOCK TABLES `ulangan` WRITE; /*!40000 ALTER TABLE `ulangan` DISABLE KEYS */; /*!40000 ALTER TABLE `ulangan` ENABLE KEYS */; UNLOCK TABLES;
--
-- TABLE structure for TABLE `users`
--

DROP TABLE IF EXISTS `users`; /*!40101

SET @saved_cs_client = @@character_set_client */; /*!40101
SET character_set_client = utf8 */;

CREATE TABLE `users` ( `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `email_verified_at` timestamp NULL DEFAULT NULL, `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, `role` enum('Admin','Guru','Siswa','Operator') COLLATE utf8mb4_unicode_ci NOT NULL, `no_induk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `id_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`), UNIQUE KEY `users_email_unique` (`email`) ) ENGINE = InnoDB AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci; /*!40101
SET character_set_client = @saved_cs_client */;
--
-- Dumping data for TABLE `users`
-- LOCK TABLES `users` WRITE; /*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin@gmail.com',NULL,'$2y$10$/9B28GC.3MSc9UsqpNbOy.teBiZXuUBJ5cBQcC641uAbUfXbMKDui','Admin',NULL,NULL,NULL,'2022-04-24 20:44:49','2022-04-24 20:44:49',NULL),(3,'Ir. Hj. Ulfah Sari','Ulfah@gmail.com',NULL,'$2y$10$Z2tueViysRgKpwzOojGe/OlKpyv5wmhLy4ENZ5tCRq62DOJIfaFMq','Guru',NULL,'00002',NULL,'2022-06-07 13:59:50','2022-06-20 14:55:58','2022-06-20 14:55:58'),(4,'ahmad zaki','AhmadZaki@gmail.com',NULL,'$2y$10$0QJ1iqxVEfrExgm48kpMfuGfqg/nTwk/GCr9OG1/ozeIycL1ekNH2','Siswa','041300',NULL,NULL,'2022-06-07 14:05:32','2022-06-07 14:05:32',NULL),(5,'Caya Murni,S.Pd','caya@gmail.com',NULL,'$2y$10$KVql.3R4/6.6n0qE97/dVeo2JanaesOtAsDD0UlCJOxC2ibi2wYlK','Guru',NULL,'00003',NULL,'2022-06-20 14:59:34','2022-06-20 14:59:34',NULL),(6,'m. yasir','yasir@gmail.com',NULL,'$2y$10$WpK20D42UT7p0kYRppmkX.ljsLoZ0ZXRn/UpNXf1oooOHrhQf.zJy','Siswa','041323',NULL,NULL,'2022-06-20 15:51:01','2022-06-20 15:51:01',NULL); /*!40000 ALTER TABLE `users` ENABLE KEYS */; UNLOCK TABLES; /*!40103

SET TIME_ZONE = @OLD_TIME_ZONE */; /*!40101
SET SQL_MODE = @OLD_SQL_MODE */; /*!40014
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */; /*!40014
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */; /*!40101
SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */; /*!40101
SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */; /*!40101
SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */; /*!40111
SET SQL_NOTES = @OLD_SQL_NOTES */;
-- Dump completed
ON 2022-07-02 2:08:48