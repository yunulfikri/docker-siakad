<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class GuruMapel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'guru_mapels';
    protected $fillable = [ 'mapel_id', 'guru_id'];

}
