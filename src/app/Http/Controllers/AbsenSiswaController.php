<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Jadwal;
use App\Siswa;
use App\AbsenSiswa;
use App\Kelas;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class AbsenSiswaController extends Controller
{
    //
    public function index()
    {
        # code...
        $kelas = Kelas::OrderBy('nama_kelas', 'asc')->get();
        return view('guru.absen.index', compact('kelas'));
    }
    public function show($id)
    {
        # code...
        $id = Crypt::decrypt($id);
        $siswa = Siswa::OrderBy('nama_siswa', 'asc')->where('kelas_id', $id)->get();
        return view('guru.absen.show', compact('siswa'));

    }
    public function more($id)
    {
        # code...
        // INNER JOIN siswa ON siswa.id = absen_siswas.siswa_id
        // INNER JOIN jadwal ON jadwal.id = absen_siswas.jadwal_id
        // INNER JOIN mapel ON mapel.id = jadwal.mapel_id
        $id = Crypt::decrypt($id);

        $absen = AbsenSiswa::select(
            'siswa.nama_siswa',
            'absen_siswas.tanggal',
            'absen_siswas.ket',
            'mapel.nama_mapel',
            'jadwal.jam_mulai',
            'jadwal.jam_selesai'

        )
        ->where('absen_siswas.siswa_id', $id)
        ->join('siswa', 'siswa.id', '=', 'absen_siswas.siswa_id')
        ->join('jadwal', 'jadwal.id', '=', 'absen_siswas.jadwal_id')
        ->join('mapel', 'mapel.id', '=', 'jadwal.mapel_id')
        ->orderBy('absen_siswas.created_at')
        ->get();

        return view('guru.absen.more', compact('absen'));

    }
    public function create($id)
    {
        # code...
        $id = Crypt::decrypt($id); #decrypt id jadwal
        $tanggal = date('Y-m-d');
        // $absen = AbsenSiswa::where('jadwal_id', $id)
        //         ->where('tanggal', $tanggal)
        //         ->exists();
        // if ($absen === true) {
        //     return redirect()->back()->with('success', 'Absensi telah tersimpan! kembali ke menu utama');

        // }
        $absen = AbsenSiswa::where('jadwal_id', $id)
                ->where('tanggal', $tanggal)
                ->join('siswa', 'siswa.id', '=', 'absen_siswas.siswa_id')
                ->get();
        $jadwal = Jadwal::findorfail($id);
        $siswa = Siswa::where('kelas_id',$jadwal->kelas_id)->get();
        
        return view('guru.absen.create', compact('siswa', 'jadwal', 'absen'));
    }

    public function store(Request $request)
    {
        # code...
      
        foreach ($request->absensi as $key => $value) {
            // return $key . $value;
            # code...
            AbsenSiswa::create([
                'siswa_id' => $key,
                'jadwal_id' => $request->jadwal_id,
                'ket' => $value,
                'tanggal' => date('Y-m-d')
            ]);
        }

        return redirect()->route('jadwal.guru')->with('success', 'Absensi telah tersimpan! kembali ke menu utama');
        
    }
}
