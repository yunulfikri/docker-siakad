<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kurikulum;
class KurikulumController extends Controller
{
    //
    public function index()
    {
        # code...
        $kurikulum = Kurikulum::all();
        return view('admin.kurikulum.index', compact('kurikulum'));
        
    }
    public function store(Request $request)
    {
        # code...
        $this->validate($request, [
            'nama' => 'required',
            'kurikulum' => 'required',
        ]);
        if ($request->kurikulum) {
            $kurikulum = $request->kurikulum;
            $new_kurikulum = date('siHdmY') . "_" . $kurikulum->getClientOriginalName();
            $kurikulum->move('uploads/kurikulum/', $new_kurikulum);
            $name_kurikulum = 'uploads/kurikulum/' . $new_kurikulum;
        } 
        Kurikulum::create([
            'nama' => $request->nama,
            'path' => $name_kurikulum,
        ]);
        return redirect()->back()->with('success', 'Data kurikulum berhasil diperbarui!');

    }
    public function destroy($id)
    {
        # code...
        $kurikulum = Kurikulum::findorfail($id);
        $kurikulum->delete();
        return redirect()->back()->with('success', 'Data kurikulum berhasil dihapus!');
        
    }
}
