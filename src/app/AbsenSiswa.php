<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbsenSiswa extends Model
{
    //
    protected $fillable = ['siswa_id', 'ket', 'tanggal', 'jadwal_id'];

    protected $table = 'absen_siswas';
}
