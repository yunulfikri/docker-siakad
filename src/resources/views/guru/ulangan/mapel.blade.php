@extends('template_backend.home')
@section('heading', 'Entry Nilai Ulangan')
@section('page')
  <li class="breadcrumb-item active">Entry Nilai Ulangan - Pilih Mata Pelajaran</li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
          <h1>Pilih mata pelajaran</h1>
          <div class="row col-12 mt-5">
            @foreach ($pelajaran as $item)
                <a href="{{ route('ulangan.index2', Crypt::encrypt($item->id)) }}">
                    <div class="card">
                        <div class="card-body">
                            <h3>{{ $item->nama_mapel }}</h3>
                        </div>
                    </div>
                </a>
            @endforeach
          </div>
        </div>
    </div>
</div>
@endsection
@section('script')
  <script>
    $("#NilaiGuru").addClass("active");
    $("#liNilaiGuru").addClass("menu-open");
    $("#UlanganGuru").addClass("active");
  </script>
@endsection