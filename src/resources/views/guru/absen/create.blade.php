@extends('template_backend.home')
@section('heading', 'Absensi Siswa')
@section('page')
  <li class="breadcrumb-item active">Absensi Siswa</li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
         <h2>Kelas : {{ $jadwal->kelas->nama_kelas }}</h2>
         <h2>Wali Kelas : {{ $jadwal->kelas->guru->nama_guru }}</h2>
         <h2>Mata Pelajaran : {{ $jadwal->mapel->nama_mapel }}</h2>
         <h2>Jam : {{ $jadwal->jam_mulai }} - {{ $jadwal->jam_selesai }}</h2>

        <form action="{{ route('absen.siswa.store') }}" method="post">
          @csrf
          <input type="hidden" name="jadwal_id" value="{{ $jadwal->id }}">
          <div class="mt-2">
            <table class="table table-striped">
              <thead>
                <th>Nama Siswa</th>
                <th>Absensi</th>
              </thead>
              <tbody>
                @foreach ($siswa as $item)
                  <tr>
                    <td>{{ $item->nama_siswa }}</td>
                    <td>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" id="" name="absensi[{{ $item->id}}]" value="hadir">
                        <label for="" class="form-check-label">Hadir</label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" id="" name="absensi[{{ $item->id}}]" value="sakit">
                        <label for="" class="form-check-label">Sakit</label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" id="" name="absensi[{{ $item->id}}]" value="izin">
                        <label for="" class="form-check-label">Izin</label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" id="" name="absensi[{{ $item->id}}]" value="tanpa keterangan">
                        <label for="" class="form-check-label">Tanpa Keterangan</label>
                      </div>
                    </td>
                  </tr>
                @endforeach
                
              </tbody>
            </table>
            <button type="submit" class="btn btn-block bg-gradient-info">Simpan Daftar Kehadiran</button>
          </div>
        </form>
        </div>
    </div>

    <div class="card">
      <div class="card-header">
        Absensi Hari Ini
      </div>
      <div class="card-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Nama Siswa</th>
              <th>Keterangan</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($absen as $item)
              <tr>
                <td>{{ $item->nama_siswa }}</td>
                <td>{{ $item->ket }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection
@section('script')
  <script>
    $("#JadwalGuru").addClass("active");
  </script>
@endsection