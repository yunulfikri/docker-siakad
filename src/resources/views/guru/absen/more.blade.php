@extends('template_backend.home')
@section('heading', 'Data Absensi Siswa')
@section('page')
  <li class="breadcrumb-item active">Data Absensi Siswa</li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Mata Pelajaran</th>
                      <th>Tanggal</th>
                      <th>Jam Pelajaran</th>
                      <th>Keterangan</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($absen as $data)
                      <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $data->nama_siswa }}</td>
                          <td>{{ $data->nama_mapel }}</td>
                          <td>{{ $data->tanggal }}</td>
                          <td>{{ $data->jam_mulai }} - {{ $data->jam_selesai }}</td>
                          <td>{{ $data->ket }}</td>
                          
                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
    </div>
</div>
@endsection
@section('script')
  <script>
    $("#AbsenSiswa").addClass("active");
  </script>
@endsection