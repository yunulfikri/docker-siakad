@extends('template_backend.home')
@section('heading', 'Data Siswa')
@section('page')
  <li class="breadcrumb-item active">Data Siswa</li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Kelas</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($kelas as $data)
                      <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $data->nama_kelas }}</td>
                          <td>
                              <a href="{{ route('absen.siswa.kelas', Crypt::encrypt($data->id)) }}" class="btn btn-info btn-sm"><i class="nav-icon fas fa-search-plus"></i> &nbsp; Ditails</a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
    </div>
</div>
@endsection
@section('script')
  <script>
    $("#AbsenSiswa").addClass("active");
  </script>
@endsection