@extends('template_backend.home')
@section('heading', 'Daftar Siswa Siswa')
@section('page')
  <li class="breadcrumb-item active">Daftar Siswa</li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Siswa</th>
                        <th>No Induk</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($siswa as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nama_siswa }}</td>
                            <td>{{ $data->no_induk }}</td>
                            <td>
                                <a href="{{ asset($data->foto) }}" data-toggle="lightbox" data-title="Foto {{ $data->nama_siswa }}" data-gallery="gallery" data-footer='<a href="{{ route('siswa.ubah-foto', Crypt::encrypt($data->id)) }}" id="linkFotoGuru" class="btn btn-link btn-block btn-light"><i class="nav-icon fas fa-file-upload"></i> &nbsp; Ubah Foto</a>'>
                                    <img src="{{ asset($data->foto) }}" width="130px" class="img-fluid mb-2">
                                </a>
                                {{-- https://siakad.didev.id/siswa/ubah-foto/{{$data->id}} --}}
                            </td>
                            <td>
                                <a href="{{ route('absen.siswa.more', Crypt::encrypt($data->id)) }}" class="btn btn-success btn-sm mt-2">Selengkapnya</a>                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
@section('script')
  <script>
    $("#AbsenSiswa").addClass("active");
  </script>
@endsection