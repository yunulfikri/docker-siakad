
<?php $__env->startSection('heading', 'Daftar Siswa Siswa'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Daftar Siswa</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Siswa</th>
                        <th>No Induk</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e($data->nama_siswa); ?></td>
                            <td><?php echo e($data->no_induk); ?></td>
                            <td>
                                <a href="<?php echo e(asset($data->foto)); ?>" data-toggle="lightbox" data-title="Foto <?php echo e($data->nama_siswa); ?>" data-gallery="gallery" data-footer='<a href="<?php echo e(route('siswa.ubah-foto', Crypt::encrypt($data->id))); ?>" id="linkFotoGuru" class="btn btn-link btn-block btn-light"><i class="nav-icon fas fa-file-upload"></i> &nbsp; Ubah Foto</a>'>
                                    <img src="<?php echo e(asset($data->foto)); ?>" width="130px" class="img-fluid mb-2">
                                </a>
                                
                            </td>
                            <td>
                                <a href="<?php echo e(route('absen.siswa.more', Crypt::encrypt($data->id))); ?>" class="btn btn-success btn-sm mt-2">Selengkapnya</a>                                
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script>
    $("#AbsenSiswa").addClass("active");
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/resources/views/guru/absen/show.blade.php ENDPATH**/ ?>