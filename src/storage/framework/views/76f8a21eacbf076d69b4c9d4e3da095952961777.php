
<?php $__env->startSection('heading', 'Details Guru'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active"><a href="<?php echo e(route('guru.index')); ?>">Guru</a></li>
  <li class="breadcrumb-item active">Details Guru</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <a href="<?php echo e(route("guru.mapel", Crypt::encrypt($guru->mapel_id))); ?>" class="btn btn-default btn-sm"><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</a>
        </div>
        <div class="card-body">
            <div class="row no-gutters ml-2 mb-2 mr-2">
                <div class="col-md-4">
                    <img src="<?php echo e(asset($guru->foto)); ?>" class="card-img img-details" alt="...">
                </div>
                <div class="col-md-1 mb-4"></div>
                <div class="col-md-7">
                    <h5 class="card-title card-text mb-2">Nama : <?php echo e($guru->nama_guru); ?></h5>
                    <h5 class="card-title card-text mb-2">NIP : <?php echo e($guru->nip); ?></h5>
                    <h5 class="card-title card-text mb-2">No Id Card : <?php echo e($guru->id_card); ?></h5>
                    
                    <h5 class="card-title card-text mb-2">Kode Jadwal : <?php echo e($guru->kode); ?></h5>
                    <?php if($guru->jk == 'L'): ?>
                        <h5 class="card-title card-text mb-2">Jenis Kelamin : Laki-laki</h5>
                    <?php else: ?>
                        <h5 class="card-title card-text mb-2">Jenis Kelamin : Perempuan</h5>
                    <?php endif; ?>
                    <h5 class="card-title card-text mb-2">Tempat Lahir : <?php echo e($guru->tmp_lahir); ?></h5>
                    <h5 class="card-title card-text mb-2">Tanggal Lahir : <?php echo e(date('l, d F Y', strtotime($guru->tgl_lahir))); ?></h5>
                    <h5 class="card-title card-text mb-2">No. Telepon : <?php echo e($guru->telp); ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("#MasterData").addClass("active");
        $("#liMasterData").addClass("menu-open");
        $("#DataGuru").addClass("active");
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/resources/views/admin/guru/details.blade.php ENDPATH**/ ?>