<?php $__env->startSection('heading', 'Entry Nilai Ulangan'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Entry Nilai Ulangan</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Entry Nilai Ulangan</h3>
      </div>
      <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <table class="table" style="margin-top: -10px;">
                    <tr>
                        <td>Nama Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->nama_kelas); ?></td>
                    </tr>
                    <tr>
                        <td>Wali Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->guru->nama_guru); ?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->count()); ?></td>
                    </tr>
                    <tr>
                        <td>Mata Pelajaran</td>
                        <td>:</td>
                        <td><?php echo e($guru->mapel->nama_mapel); ?></td>
                    </tr>
                    <tr>
                        <td>Guru Mata Pelajaran</td>
                        <td>:</td>
                        <td><?php echo e($guru->nama_guru); ?></td>
                    </tr>
                    <?php
                        $bulan = date('m');
                        $tahun = date('Y');
                    ?>
                    <tr>
                        <td>Semester</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e('Semester Ganjil'); ?>

                            <?php else: ?>
                                <?php echo e('Semester Genap'); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun Pelajaran</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e($tahun); ?>/<?php echo e($tahun+1); ?>

                            <?php else: ?>
                                <?php echo e($tahun-1); ?>/<?php echo e($tahun); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="ctr">No.</th>
                            <th>Nama Siswa</th>
                            <th class="ctr">ULHA 1</th>
                            <th class="ctr">ULHA 2</th>
                            <th class="ctr">UTS</th>
                            <th class="ctr">ULHA 3</th>
                            <th class="ctr">UAS</th>
                            <th class="ctr">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="" method="post">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="guru_id" value="<?php echo e($guru->id); ?>">
                            <input type="hidden" name="kelas_id" value="<?php echo e($kelas->id); ?>">
                            <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <input type="hidden" name="siswa_id" value="<?php echo e($data->id); ?>">
                                <tr>
                                    <td class="ctr"><?php echo e($loop->iteration); ?></td>
                                    <td>
                                        <?php echo e($data->nama_siswa); ?>

                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['id']): ?>
                                            <input type="hidden" name="ulangan_id" class="ulangan_id_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)->id); ?>">
                                        <?php else: ?>
                                            <input type="hidden" name="ulangan_id" class="ulangan_id_<?php echo e($data->id); ?>" value="">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr">
                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['ulha_1']): ?>
                                            <div class="text-center"><?php echo e($data->ulangan($data->id)['ulha_1']); ?></div>
                                            <input type="hidden" name="ulha_1" class="ulha_1_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)['ulha_1']); ?>">
                                        <?php else: ?>
                                            <input type="text" name="ulha_1" maxlength="2" onkeypress="return inputAngka(event)" style="margin: auto;" class="form-control text-center ulha_1_<?php echo e($data->id); ?>" autocomplete="off">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr">
                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['ulha_2']): ?>
                                            <div class="text-center"><?php echo e($data->ulangan($data->id)['ulha_2']); ?></div>
                                            <input type="hidden" name="ulha_2" class="ulha_2_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)['ulha_2']); ?>">
                                        <?php else: ?>
                                            <input type="text" name="ulha_2" maxlength="2" onkeypress="return inputAngka(event)" style="margin: auto;" class="form-control text-center ulha_2_<?php echo e($data->id); ?>" autocomplete="off">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr">
                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['uts']): ?>
                                            <div class="text-center"><?php echo e($data->ulangan($data->id)['uts']); ?></div>
                                            <input type="hidden" name="uts" class="uts_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)['uts']); ?>">
                                        <?php else: ?>
                                            <input type="text" name="uts" maxlength="2" onkeypress="return inputAngka(event)" style="margin: auto;" class="form-control text-center uts_<?php echo e($data->id); ?>" autocomplete="off">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr">
                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['ulha_3']): ?>
                                            <div class="text-center"><?php echo e($data->ulangan($data->id)['ulha_3']); ?></div>
                                            <input type="hidden" name="ulha_3" class="ulha_3_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)['ulha_3']); ?>">
                                        <?php else: ?>
                                            <input type="text" name="ulha_3" maxlength="2" onkeypress="return inputAngka(event)" style="margin: auto;" class="form-control text-center ulha_3_<?php echo e($data->id); ?>" autocomplete="off">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr">
                                        <?php if($data->ulangan($data->id) && $data->ulangan($data->id)['uas']): ?>
                                            <div class="text-center"><?php echo e($data->ulangan($data->id)['uas']); ?></div>
                                            <input type="hidden" name="uas" class="uas_<?php echo e($data->id); ?>" value="<?php echo e($data->ulangan($data->id)['uas']); ?>">
                                        <?php else: ?>
                                            <input type="text" name="uas" maxlength="2" onkeypress="return inputAngka(event)" style="margin: auto;" class="form-control text-center uas_<?php echo e($data->id); ?>" autocomplete="off">
                                        <?php endif; ?>
                                    </td>
                                    <td class="ctr sub_<?php echo e($data->id); ?>">
                                        <?php if($data->nilai($data->id)): ?>
                                            <i class="fas fa-check" style="font-weight:bold;"></i>
                                        <?php else: ?>
                                            <button type="button" id="submit-<?php echo e($data->id); ?>" class="btn btn-default btn_click" data-id="<?php echo e($data->id); ?>"><i class="nav-icon fas fa-save"></i></button>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </form>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(".btn_click").click(function(){
            var id = $(this).attr('data-id');
            var ulha_1 = $(".ulha_1_"+id).val();
            var ulha_2 = $(".ulha_2_"+id).val();
            var uts = $(".uts_"+id).val();
            var ulha_3 = $(".ulha_3_"+id).val();
            var uas = $(".uas_"+id).val();
            var ulangan_id = $(".ulangan_id_"+id).val();
            var guru_id = $("input[name=guru_id]").val();
            var kelas_id = $("input[name=kelas_id]").val();
            
            $.ajax({
                url: "<?php echo e(route('ulangan.store')); ?>",
                type: "POST",
                dataType: 'json',
                data 	: {
                    _token: '<?php echo e(csrf_token()); ?>',
                    id : ulangan_id,
                    siswa_id : id,
                    kelas_id : kelas_id,
                    guru_id : guru_id,
                    ulha_1 : ulha_1,
                    ulha_2 : ulha_2,
                    uts : uts,
                    ulha_3 : ulha_3,
                    uas : uas,
                },
                success: function(data){
                    toastr.success("Nilai ulangan siswa berhasil ditambahkan!");
                    location.reload();
                },
                error: function (data) {
                    toastr.warning("Errors 404!");
                }
            });
        });
        
        $("#NilaiGuru").addClass("active");
        $("#liNilaiGuru").addClass("menu-open");
        $("#UlanganGuru").addClass("active");
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/yunulfikri/Documents/Tasks/Sistem-Informasi-Akademik-Sekolah-Laravel/resources/views/guru/ulangan/nilai.blade.php ENDPATH**/ ?>