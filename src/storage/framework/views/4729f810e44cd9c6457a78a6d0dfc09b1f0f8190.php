
<?php $__env->startSection('heading', 'Details Siswa'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active"><a href="<?php echo e(route('siswa.index')); ?>">Siswa</a></li>
  <li class="breadcrumb-item active">Details Siswa</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <a href="<?php echo e(route('siswa.kelas', Crypt::encrypt($siswa->kelas_id))); ?>" class="btn btn-default btn-sm"><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</a>
        </div>
        <div class="card-body">
            <div class="row no-gutters ml-2 mb-2 mr-2">
                <div class="col-md-4">
                    <img src="<?php echo e(asset($siswa->foto)); ?>" class="card-img img-details" alt="...">
                </div>
                <div class="col-md-1 mb-4"></div>
                <div class="col-md-7">
                    <h5 class="card-title card-text mb-2">Nama : <?php echo e($siswa->nama_siswa); ?></h5>
                    <h5 class="card-title card-text mb-2">No. Induk : <?php echo e($siswa->no_induk); ?></h5>
                    <h5 class="card-title card-text mb-2">NIS : <?php echo e($siswa->nis); ?></h5>
                    <h5 class="card-title card-text mb-2">Kelas : <?php echo e($siswa->kelas->nama_kelas); ?></h5>
                    <?php if($siswa->jk == 'L'): ?>
                        <h5 class="card-title card-text mb-2">Jenis Kelamin : Laki-laki</h5>
                    <?php else: ?>
                        <h5 class="card-title card-text mb-2">Jenis Kelamin : Perempuan</h5>
                    <?php endif; ?>
                    <h5 class="card-title card-text mb-2">Tempat Lahir : <?php echo e($siswa->tmp_lahir); ?></h5>
                    <h5 class="card-title card-text mb-2">Tanggal Lahir : <?php echo e(date('l, d F Y', strtotime($siswa->tgl_lahir))); ?></h5>
                    <h5 class="card-title card-text mb-2">No. Telepon : <?php echo e($siswa->telp); ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("#MasterData").addClass("active");
        $("#liMasterData").addClass("menu-open");
        $("#DataSiswa").addClass("active");
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/resources/views/admin/siswa/details.blade.php ENDPATH**/ ?>