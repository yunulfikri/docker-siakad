<?php $__env->startSection('heading', 'Show Rapot'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Show Rapot</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Show Rapot</h3>
      </div>
      <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <table class="table" style="margin-top: -10px;">
                    <tr>
                        <td>No Induk Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->no_induk); ?></td>
                    </tr>
                    <tr>
                        <td>Nama Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->nama_siswa); ?></td>
                    </tr>
                    <tr>
                        <td>Nama Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->nama_kelas); ?></td>
                    </tr>
                    <tr>
                        <td>Wali Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->guru->nama_guru); ?></td>
                    </tr>
                    <?php
                        $bulan = date('m');
                        $tahun = date('Y');
                    ?>
                    <tr>
                        <td>Semester</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e('Semester Ganjil'); ?>

                            <?php else: ?>
                                <?php echo e('Semester Genap'); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun Pelajaran</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e($tahun); ?>/<?php echo e($tahun+1); ?>

                            <?php else: ?>
                                <?php echo e($tahun-1); ?>/<?php echo e($tahun); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="ctr" rowspan="2">No.</th>
                            <th rowspan="2">Mata Pelajaran</th>
                            <th class="ctr" colspan="3">Pengetahuan</th>
                            <th class="ctr" colspan="3">Keterampilan</th>
                        </tr>
                        <tr>
                            <th class="ctr">Nilai</th>
                            <th class="ctr">Predikat</th>
                            <th class="ctr">Deskripsi</th>
                            <th class="ctr">Nilai</th>
                            <th class="ctr">Predikat</th>
                            <th class="ctr">Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php $__currentLoopData = $mapel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $data = $data[0]; ?>
                                <tr>
                                    <td><?php echo e($loop->iteration); ?></td>
                                    <td><?php echo e($data->mapel->nama_mapel); ?></td>
                                    <?php
                                        $array = array('mapel' => $val, 'siswa' => $siswa->id);
                                        $jsonData = json_encode($array);
                                    ?>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['p_nilai']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['p_predikat']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['p_deskripsi']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['k_nilai']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['k_predikat']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekRapot($jsonData)['k_deskripsi']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("#Nilai").addClass("active");
        $("#liNilai").addClass("menu-open");
        $("#Rapot").addClass("active");
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/yunulfikri/Documents/Tasks/Sistem-Informasi-Akademik-Sekolah-Laravel/resources/views/admin/rapot/show.blade.php ENDPATH**/ ?>