<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Akademik Sekolah</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo e(asset('dist/css/adminlte.min.css')); ?>" rel="stylesheet" />

        <script src="<?php echo e(asset('plugins/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>


    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container px-5">
                <a class="navbar-brand" href="#!">SIAKAD</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('login')); ?>">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('register')); ?>">Register</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page Content-->
        <div class="container px-4 px-lg-5">
            <!-- Heading Row-->
            <div class="row gx-4 gx-lg-5 align-items-center my-5">
                <div class="col-lg-7">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100" src="<?php echo e(asset('img/fotosekolah.jpg')); ?>" alt="First slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo e(asset('img/slider (1).jpg')); ?>" alt="First slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo e(asset('img/slider (2).jpg')); ?>" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo e(asset('img/slider (3).jpg')); ?>" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
                </div>
                <div class="col-lg-5">
                    <h1 class="font-weight-light">SMKN 4 BANDA ACEH</h1>
                    <p>
                    Salah satu satuan pendidikan dengan jenjang SMK di Mulia, Kec. Kuta Alam, Kota Banda Aceh, Aceh. Dalam menjalankan kegiatannya, SMKN 4 BANDA ACEH berada di bawah naungan Kementerian Pendidikan dan Kebudayaan. SMKN 4 BANDA ACEH beralamat di SISINGAMANGARAJA NO.109, Mulia, Kec. Kuta Alam, Kota Banda Aceh, Aceh, dengan kode pos 23123.
                    </p>
                    <p>Akreditasi : B <br>
                        Kepala Sekolah : Yasni Marjaya</p>
                </div>
            </div>
            <!-- Call to Action-->
            <div class="card text-white bg-info my-5 py-4 text-center">
                <div class="card-body">
                    <h2>Visi</h2>
                    <p class="text-white m-0">Menjalankan SMK yang berkualitas, unggul berlandaskan IMTAQ dan IPTEK serta menghasilkan tamatan yang mampu bersaing ditingkat nasional dan internasional</p>
                    <hr>
                    <h2>Misi</h2>
                    <p class="text-white m-0">
                        <ol>
                            <li>Menyiapkan  tenaga  kerja  profesional  yang  mandiri,  produktif,  terampil,kreatif serta inovatif.</li>
                            <li>Sebagai pusat pengembangan pendidikan kejuruan yang dapat dipercaya oleh masyarakat,  dunia usaha dan  industri  secara nasional  dan  regional  serta internasional</li>
                            <li>Melayani masyarakat untuk mendapatkan ketrampilan kerja guna memasuki dunia kerja dan wirausaha </li>
                            <li>Melayani siswa dan masyarakat yang berminat untuk mengikuti uji profesi dalam bidang keterampilan </li>

                        </ol>
                    </p>
                </div>
            </div>
            <!-- Content Row-->
            <div class="row gx-4 gx-lg-5">
                <div class="col-md-4 mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title font-weight-bold">Nautika Kapal Penangkap Ikan</h2>
                            <p class="card-text mt-5">Membekali peserta didik dengan keterampilan, pengetahuan, dan sikap agar kompeten dalam: Melakukan pekerjaan sebagai Anak Buah Kapal (ABK) Perikanan bagian deck yang profesional dalam bidang perawatan kapal ikan dan alat tangkapnya.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title font-weight-bold">Teknik Kendaraan Ringan Otomotif</h2>
                            <p class="card-text mt-5">Teknik Kendaraan Ringan Otomotif merupakan kompetensi keahlian bidang teknik otomotif yang menekankan keahlian pada bidang penguasaan jasa perbaikan pada kendaraan ringan mobil dan sepeda motor.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title font-weight-bold">Teknik Bisnis Sepeda Motor</h2>
                            <p class="card-text mt-5">Teknik dan Bisnis Sepeda Motor (TBSM) adalah salah satu cabang ilmu teknik mesin yang mempelajari tentang bagaimana merancang, membuat dan mengembangkan alat-alat transportasi darat yang menggunakan mesin, terutama sepeda motor.</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Copyright &copy; SMKN 4 Banda Aceh 2021</p></div>
        </footer>

        <script>
            $('.carousel').carousel({
  interval: 2000
});
        </script>
    </body>
</html>
<?php /**PATH /var/www/resources/views/landingpage.blade.php ENDPATH**/ ?>