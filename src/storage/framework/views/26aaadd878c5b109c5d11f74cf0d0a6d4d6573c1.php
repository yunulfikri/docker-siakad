<?php $__env->startSection('heading', 'Entry Nilai Rapot'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Entry Nilai Rapot</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Entry Nilai Rapot</h3>
      </div>
      <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <table class="table" style="margin-top: -10px;">
                    <tr>
                        <td>Nama Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->nama_kelas); ?></td>
                    </tr>
                    <tr>
                        <td>Wali Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->guru->nama_guru); ?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->count()); ?></td>
                    </tr>
                    <tr>
                        <td>Mata Pelajaran</td>
                        <td>:</td>
                        <td><?php echo e($guru->mapel->nama_mapel); ?></td>
                    </tr>
                    <tr>
                        <td>Guru Mata Pelajaran</td>
                        <td>:</td>
                        <td><?php echo e($guru->nama_guru); ?></td>
                    </tr>
                    <?php
                        $bulan = date('m');
                        $tahun = date('Y');
                    ?>
                    <tr>
                        <td>Semester</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e('Semester Ganjil'); ?>

                            <?php else: ?>
                                <?php echo e('Semester Genap'); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun Pelajaran</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e($tahun); ?>/<?php echo e($tahun+1); ?>

                            <?php else: ?>
                                <?php echo e($tahun-1); ?>/<?php echo e($tahun); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="ctr" rowspan="2">No.</th>
                            <th rowspan="2">Nama Siswa</th>
                            <th class="ctr" colspan="3">Pengetahuan</th>
                            <th class="ctr" colspan="3">Keterampilan</th>
                            <th class="ctr" rowspan="2">Aksi</th>
                        </tr>
                        <tr>
                            <th class="ctr">Nilai</th>
                            <th class="ctr">Predikat</th>
                            <th class="ctr">Deskripsi</th>
                            <th class="ctr">Nilai</th>
                            <th class="ctr">Predikat</th>
                            <th class="ctr">Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="" method="post" id="formRapot">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="guru_id" value="<?php echo e($guru->id); ?>">
                            <input type="hidden" name="kelas_id" value="<?php echo e($kelas->id); ?>">
                            <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <input type="hidden" name="siswa_id" value="<?php echo e($data->id); ?>">
                                <tr>
                                    <td class="ctr"><?php echo e($loop->iteration); ?></td>
                                    <td><?php echo e($data->nama_siswa); ?></td>
                                    <?php if($data->nilai($data->id)): ?>
                                        <td class="ctr">
                                            <input type="hidden" class="rapot_<?php echo e($data->id); ?>" value="<?php echo e($data->nilai($data->id)->id); ?>">
                                            <div class="text-center"><?php echo e($data->nilai($data->id)->p_nilai); ?></div>
                                        </td>
                                        <td class="ctr">
                                            <div class="text-center"><?php echo e($data->nilai($data->id)->p_predikat); ?></div>
                                        </td>
                                        <td class="ctr">
                                            <textarea class="form-control swal2-textarea textarea-rapot" cols="50" rows="5" disabled><?php echo e($data->nilai($data->id)->p_deskripsi); ?></textarea>
                                        </td>
                                        <?php if($data->nilai($data->id)->p_nilai && $data->nilai($data->id)->k_nilai): ?>
                                            <td class="ctr">
                                                <div class="ka_<?php echo e($data->id); ?> text-center"><?php echo e($data->nilai($data->id)->k_nilai); ?></div> 
                                            </td>
                                            <td class="ctr">
                                                <div class="kp_<?php echo e($data->id); ?> text-center"><?php echo e($data->nilai($data->id)->k_predikat); ?></div>
                                            </td>
                                            <td class="ctr">
                                                <textarea class="form-control swal2-textarea textarea-rapot" cols="50" rows="5" disabled><?php echo e($data->nilai($data->id)->k_deskripsi); ?></textarea>
                                            </td>
                                            <td class="ctr">
                                                <i class="fas fa-check" style="font-weight:bold;"></i>
                                            </td>
                                        <?php else: ?>
                                            <td class="ctr">
                                                <input type="text" name="nilai" maxlength="2" onkeypress="return inputAngka(event)" class="form-control text-center nilai_<?php echo e($data->id); ?>" data-ids="<?php echo e($data->id); ?>" autofocus autocomplete="off">
                                                <div class="knilai_<?php echo e($data->id); ?> text-center"></div>
                                            </td>
                                            <td class="ctr">
                                                <input type="text" name="predikat" class="form-control text-center predikat_<?php echo e($data->id); ?>" disabled>
                                                <div class="kpredikat_<?php echo e($data->id); ?> text-center"></div>
                                            </td>
                                            <td class="ctr">
                                                <textarea class="form-control swal2-textarea textarea-rapot deskripsi_<?php echo e($data->id); ?>" cols="50" rows="5" disabled></textarea>
                                            </td>
                                            <td class="ctr sub_<?php echo e($data->id); ?>">
                                                <button type="button" id="submit-<?php echo e($data->id); ?>" class="btn btn-default btn_click" data-id="<?php echo e($data->id); ?>"><i class="nav-icon fas fa-save"></i></button>
                                            </td>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <td class="ctr">
                                            <div class="text-center"></div>
                                        </td>
                                        <td class="ctr">
                                            <div class="text-center"></div>
                                        </td>
                                        <td class="ctr">
                                            <textarea class="form-control swal2-textarea textarea-rapot" cols="50" rows="5" disabled></textarea>
                                        </td>
                                        <td class="ctr">
                                            <input type="text" name="nilai" maxlength="2" onkeypress="return inputAngka(event)" class="form-control text-center nilai_<?php echo e($data->id); ?>" data-ids="<?php echo e($data->id); ?>" autofocus autocomplete="off">
                                            <div class="knilai_<?php echo e($data->id); ?> text-center"></div>
                                        </td>
                                        <td class="ctr">
                                            <input type="text" name="predikat" class="form-control text-center" disabled>
                                        </td>
                                        <td class="ctr">
                                            <textarea class="form-control swal2-textarea textarea-rapot" cols="50" rows="5" disabled></textarea>
                                        </td>
                                        <td class="ctr">
                                            <i class="fas fa-exclamation-triangle" style="font-weight:bold;"></i>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </form>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("input[name=nilai]").keyup(function(){
            var id = $(this).attr('data-ids');
		    var guru_id = $("input[name=guru_id]").val();
            var angka = $(".nilai_"+id).val();
            if (angka.length == 2){
                $.ajax({
                    type:"GET",
                    data: {
                        id : guru_id,
                        nilai : angka
                    },
                    dataType:"JSON",
                    url:"<?php echo e(url('/rapot/predikat')); ?>",
                    success:function(data){
                        $(".predikat_"+id).val(data[0]['predikat']);
                        $(".deskripsi_"+id).val(data[0]['deskripsi']);
                    },
                    error:function(){
                        toastr.warning("Tolong masukkan nilai kkm & predikat!");
                    }
                });
            } else {
                $(".predikat_"+id).val("");
                $(".deskripsi_"+id).val("");
            }
        });

        $(".btn_click").click(function(){
            var id = $(this).attr('data-id');
            var rapot = $(".rapot_"+id).val();
            var nilai = $(".nilai_"+id).val();
            var predikat = $(".predikat_"+id).val();
            var deskripsi = $(".deskripsi_"+id).val();
            var guru_id = $("input[name=guru_id]").val();
            var kelas_id = $("input[name=kelas_id]").val();
            var ok = ('<i class="fas fa-check" style="font-weight:bold;"></i>')

            if (nilai == "") {
                toastr.error("Form tidak boleh ada yang kosong!");
            } else {
                $.ajax({
                    url: "<?php echo e(route('rapot.store')); ?>",
                    type: "POST",
                    dataType: 'json',
                    data 	: {
                        _token: '<?php echo e(csrf_token()); ?>',
                        id : rapot,
                        siswa_id : id,
                        kelas_id : kelas_id,
                        guru_id : guru_id,
                        nilai : nilai,
                        predikat : predikat,
                        deskripsi : deskripsi,
                    },
                    success: function(data){
                        $(".nilai_"+id).remove();
                        $(".predikat_"+id).remove();
                        $("#submit-"+id).remove();
                        $(".knilai_"+id).append(nilai);
                        $(".kpredikat_"+id).append(predikat);
                        $(".sub_"+id).append(ok);
                        toastr.success("Nilai rapot siswa berhasil ditambahkan!");
                    },
                    error: function (data) {
                        toastr.warning("Errors 404!");
                    }
                });
            }
        });

        $("#NilaiGuru").addClass("active");
        $("#liNilaiGuru").addClass("menu-open");
        $("#RapotGuru").addClass("active");
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/yunulfikri/Documents/Tasks/Sistem-Informasi-Akademik-Sekolah-Laravel/resources/views/guru/rapot/rapot.blade.php ENDPATH**/ ?>