<?php $__env->startSection('heading', 'Show Nilai Sikap'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Show Nilai Sikap</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Show Nilai Sikap</h3>
      </div>
      <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <table class="table" style="margin-top: -10px;">
                    <tr>
                        <td>No Induk Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->no_induk); ?></td>
                    </tr>
                    <tr>
                        <td>Nama Siswa</td>
                        <td>:</td>
                        <td><?php echo e($siswa->nama_siswa); ?></td>
                    </tr>
                    <tr>
                        <td>Nama Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->nama_kelas); ?></td>
                    </tr>
                    <tr>
                        <td>Wali Kelas</td>
                        <td>:</td>
                        <td><?php echo e($kelas->guru->nama_guru); ?></td>
                    </tr>
                    <?php
                        $bulan = date('m');
                        $tahun = date('Y');
                    ?>
                    <tr>
                        <td>Semester</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e('Semester Ganjil'); ?>

                            <?php else: ?>
                                <?php echo e('Semester Genap'); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun Pelajaran</td>
                        <td>:</td>
                        <td>
                            <?php if($bulan > 6): ?>
                                <?php echo e($tahun); ?>/<?php echo e($tahun+1); ?>

                            <?php else: ?>
                                <?php echo e($tahun-1); ?>/<?php echo e($tahun); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" class="ctr">No.</th>
                            <th rowspan="2">Nama Siswa</th>
                            <th colspan="3" class="ctr">Nilai Sikap</th>
                        </tr>
                        <tr>
                            <th class="ctr">Teman</th>
                            <th class="ctr">Sendiri</th>
                            <th class="ctr">Guru</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php $__currentLoopData = $mapel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($loop->iteration); ?></td>
                                    <td><?php echo e($data->nama_mapel); ?></td>
                                    <?php
                                        $array = array('mapel' => $data->id, 'siswa' => $siswa->id);
                                        $jsonData = json_encode($array);
                                    ?>
                                    <td class="ctr"><?php echo e($data->cekSikap($jsonData)['sikap_1']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekSikap($jsonData)['sikap_2']); ?></td>
                                    <td class="ctr"><?php echo e($data->cekSikap($jsonData)['sikap_3']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("#Nilai").addClass("active");
        $("#liNilai").addClass("menu-open");
        $("#Sikap").addClass("active");
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/yunulfikri/Documents/Tasks/Sistem-Informasi-Akademik-Sekolah-Laravel/resources/views/admin/sikap/show.blade.php ENDPATH**/ ?>