
<?php $__env->startSection('heading', 'Data Absensi Siswa'); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Data Absensi Siswa</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Mata Pelajaran</th>
                      <th>Tanggal</th>
                      <th>Jam Pelajaran</th>
                      <th>Keterangan</th>
                  </tr>
              </thead>
              <tbody>
                  <?php $__currentLoopData = $absen; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                          <td><?php echo e($loop->iteration); ?></td>
                          <td><?php echo e($data->nama_siswa); ?></td>
                          <td><?php echo e($data->nama_mapel); ?></td>
                          <td><?php echo e($data->tanggal); ?></td>
                          <td><?php echo e($data->jam_mulai); ?> - <?php echo e($data->jam_selesai); ?></td>
                          <td><?php echo e($data->ket); ?></td>
                          
                      </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script>
    $("#AbsenSiswa").addClass("active");
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/resources/views/guru/absen/more.blade.php ENDPATH**/ ?>