
<?php $__env->startSection('heading', 'Jadwal Guru'); ?>
<?php $__env->startSection('heading'); ?>
    Jadwal Guru <?php echo e(Auth::user()->guru(Auth::user()->id_card)->nama_guru); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page'); ?>
  <li class="breadcrumb-item active">Jadwal Guru</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
          <table id="example2" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Hari</th>
                    <th>Kelas</th>
                    <th>Jam Mengajar</th>
                    <th>Ruang Kelas</th>
                    <th>Absensi Siswa</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $jadwal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($data->hari->nama_hari); ?></td>
                    <td><?php echo e($data->kelas->nama_kelas); ?></td>
                    <td><?php echo e($data->jam_mulai); ?> - <?php echo e($data->jam_selesai); ?></td>
                    <td><?php echo e($data->ruang->nama_ruang); ?></td>
                    <td>
                        <a class="btn btn-primary" href="<?php echo e(route('absen.siswa.create', Crypt::encrypt($data->id))); ?>">Mulai</a>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $("#JadwalGuru").addClass("active");
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template_backend.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/resources/views/guru/jadwal.blade.php ENDPATH**/ ?>